﻿namespace ClickPortal.Tests
{
    public class TestOptions
    {      
        public string AdministratorUsername { get; set; }
        public string AdministratorPassword { get; set; }

        public string BaseUrl { get; set; }

        public string ChromeDriverPath { get; set; } 

        public TestOptions(string baseUrl)
        {
            BaseUrl = baseUrl;
            
        }
    }
}
