﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class BooleanRadioButtons : ClickControl
    {
        public BooleanRadioButtons(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }

        public override void SetValue(string value)
        {
            var radio = Container.FindElement(By.CssSelector("input[type='radio'][value='" + value.ToLower() + "']"));
            radio.Click();
        }

        public void SetValue(bool value)
        {
            var strValue = "no";
            if (value)
            {
                strValue = "yes";
            }
            SetValue(strValue);
        }
    }
}
