﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class EntityTableSelection : ClickControl
    {
        public EntityTableSelection(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }

        public override void SetValue(string value)
        {
            var displayElement = Container.FindElement(By.LinkText(value));
            var row = displayElement.FindElement(By.XPath("../.."));
            var checkbox = row.FindElement(By.CssSelector("input[type='checkbox']"));
            checkbox.Click();
        }

        public void SetValue(string[] value)
        {
            if (value != null)
            {
                var numValues = value.Length;
                for (var i = 0; i < numValues; i++)
                {
                    SetValue(value[i]);
                }
            }
        }
    }
}
