﻿using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.Controls
{
    public class SingleEntityDropdownList : ClickControl
    {
        public override void SetValue(string value)
        {

            var selectOption = Container.FindElement(By.TagName("select"));
            var select = new SelectElement(selectOption);
            select.SelectByText(value);
        }

        public SingleEntityDropdownList(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }
    }
}
