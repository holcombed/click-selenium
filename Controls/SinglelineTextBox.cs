﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class SinglelineTextBox : ClickControl
    {

        public override void SetValue(string value)
        {
            var element = Container.FindElement(By.ClassName("inputControl"));
            element.Clear();
            element.SendKeys(value);
        }

        public SinglelineTextBox(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }
    }
}
