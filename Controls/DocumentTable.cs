﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class DocumentTable : ClickControl
    {
        private IWebElement AddButton { get; set; }
        public DocumentTable(PageController currentPage, string caption) : base(currentPage, caption)
        {
            AddButton = Container.FindElement(By.CssSelector("input[value='Add'][id$='_addBtn']"));
        }

        public override void SetValue(string value)
        {
            throw new System.NotImplementedException();
        }

        public void AddDocument(string title, string filePath)
        {
            CurrentPage.OpenPopup(AddButton);
            var titleInput = Driver.FindElement(By.Name("Document.name"));
            titleInput.SendKeys(title);

            var chooseFileButton = Driver.FindElement(By.Name("Document.targetURL"));

            chooseFileButton.SendKeys(filePath);

            var okButton = Driver.FindElement(By.Id("OK"));
            okButton.Click();
            CurrentPage.FocusRootWindow();
        }
    }
}
