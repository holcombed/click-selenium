﻿using System;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.Controls
{
    public abstract class ClickControl
    {
        protected readonly PageController CurrentPage;
        protected readonly IWebDriver Driver;
        protected readonly string Caption;
        private IWebElement _container;

        public IWebElement Container
        {
            get
            {
                if (_container == null && string.IsNullOrEmpty(Caption) != true)
                {
                    _container = FindContainerSpanByCaption(Caption);
                }
                return _container;
            }
            set { _container = value; }
        }

        protected ClickControl(PageController currentPage, string caption)
        {
            CurrentPage = currentPage;
            Driver = PageController.driver;
            Caption = caption;
        }

        private IWebElement FindContainerSpanByCaption(string propertyLabel)
        {
            var captionSpan = Driver.FindElement(By.XPath("//span[@class='PrintQuestion'][contains(.,'" + propertyLabel + "')]"));
            var container = captionSpan.FindElement(By.XPath(".."));
            return container;
        }

        public abstract void SetValue(string value);

        protected void WaitForElementToBeVisible(IWebElement element)
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(PageController.PAGE_WAIT_TIMEOUT)).Until(ElementIsVisible(element));
        }

        private static Func<IWebDriver, IWebElement> ElementIsVisible(IWebElement element)
        {
            return driver =>
            {
                return (element != null && element.Displayed) ? element : null;
            };
        }
    }
}
