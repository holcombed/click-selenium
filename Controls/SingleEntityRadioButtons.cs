﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class SingleEntityRadioButtons : ClickControl
    {
        public override void SetValue(string value)
        {
            var td = Container.FindElement(By.XPath("//td[text()='" + value + "']"));
            var tr = td.FindElement(By.XPath(".."));
            var radio = tr.FindElement(By.CssSelector("input[type='radio']"));
            radio.Click();
        }

        public SingleEntityRadioButtons(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }
    }
}
