﻿namespace ClickPortal
{
    public class Organization : ClickEType
    {
        [ETypeProperty("ID")]
        public string ID;

        [ETypeProperty("name")]
        public string Name;

        [ETypeProperty("companyCategory", RelatedEType="CompanyCategory")]
        public string Category;

        public Organization() : base("Company")
        {
            ID = "TestOrg";
            Name = "Test Organization";
            Category = "Department";
        }

        public Organization(string id, string name, string category) : base("Company")
        {
            this.ID = id;
            this.Name = name;
            this.Category = category;
        }
    }
}
