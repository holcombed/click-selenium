﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace ClickPortal.PortalDriver {
    class PortalConsole : IPortalConsole
    {

        private PortalDriver portalDriver;
        private string urlOrLabel;
        private const string ConsolePath = "/DebugConsole/CommandWindow/Command";

        internal PortalConsole(PortalDriver portalDriver, string urlOrLabel)
        {
            this.portalDriver = portalDriver;
            this.urlOrLabel = urlOrLabel;
        }

        public bool Go()
        {
            var url = this.portalDriver.getBaseURL(this.urlOrLabel);
            this.portalDriver.Navigate().GoToUrl(url + ConsolePath + "?_webrNew=All");
            return this.Is();
        }

        public bool Is()
        {
            return (this.portalDriver.Url.ToLower().Contains(ConsolePath.ToLower()));
        }

        public string Execute(string script)
        {
            //this.portalDriver.FindElement(By.Name("CommandScript.script")).SendKeys(script);

            ((IJavaScriptExecutor)this.portalDriver).ExecuteScript(@"document.getElementById('CommandScript.script').value = arguments[0];", script);
            this.portalDriver.FindElement(By.Id("Run")).Click();
            Thread.Sleep(500);
            return this.portalDriver.FindElement(By.Name("OutputScript")).GetAttribute("value");
        }
    }
}