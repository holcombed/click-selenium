﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperChooser : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver) {

            if (helper.type == "Chooser") return new ControlChooser(helper, driver);
            return null;
        }

        internal class ControlChooser : IPortalControl {

            private readonly ControlHelper helper;
            private IPortalDriver driver;

            internal ControlChooser(ControlHelper helper, IPortalDriver driver) {
                this.helper = helper;
                this.driver = driver;
            }

            public bool Is(IPortalControlValue value) {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public bool Is(string textOrOID) {
                var text = textOrOID ?? "";
                if (text == "" && this.helper.text == "") return true;
                return this.helper.text.Trim().Contains(text.Trim());
            }

            public string OID {
                get { return this.helper.OID; }
            }

            public string Text {
                get { return this.helper.text; }
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public bool IsClear {
                get { return this.helper.OID == null || this.helper.OID.Trim() == ""; }
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                return this.helper.Values.GetEnumerator();
            }


            IEnumerator IEnumerable.GetEnumerator() {
                return this.GetEnumerator();
            }

            public IPortalControlValue this[int index] {
                get { return this.helper.Values[index]; }
            }

            public int Count {
                get { return this.helper.Values.Count; }
            }

            public bool Is() {
                return true;
            }

            public IPortalControlValue Clear()
            {
                if (!this.IsClear) this.helper.widgets[1].Click(); 
                return null;
            }

            public IPortalControlValue Val() {
                if (this.IsClear) new PortalControlValue(null, "", true);
                return new PortalControlValue(this.helper.OID, this.helper.text.Trim(), false);
            }

            public IPortalControlValue Val(string TextOrOID) {

                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                if (helper.widgets[0].GetAttribute("type") != "button")
                {
                    this.helper.widgets[0].SendKeys(TextOrOID);
                    var firstChoice = By.CssSelector("tr.resultItem:nth-of-type(1)");
                    new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(drv => drv.FindElement(firstChoice)).Click();
                }
                else
                {
                    driver.Window().New(this.helper.widgets[0].Click);
                    // Check to see if the desired item is already displayed
                    try
                    {
                        driver.FindElement(By.XPath("//div[@id='_webrRSV_DIV_0']//tbody/tr[td[2][contains(text(),'" + TextOrOID + "')]]/td[1]/input[@type='radio']")).Click();
                    }
                    catch (NoSuchElementException)
                    {
                        driver.FindElement(By.Id("_webrRSV_FilterValue_0_0")).SendKeys(TextOrOID);
                        driver.FindElement(By.XPath("//input[@type='button' and @value='Go']")).Click();
                    }
                    finally
                    {
                        new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementExists(By.XPath("//div[@id='_webrRSV_DIV_0']//tbody/tr[td[2][contains(text(),'" + TextOrOID + "')]]/td[1]/input[@type='radio']"))).Click();
                        Thread.Sleep(500);
                    }

                    driver.Window().Close(driver.FindElement(By.Id("btnOk")).Click);
                }

                return null;
            }

            public IPortalControlValue Val(DateTime date)
            {
                this.Val(date.ToString());
                return null;
            }

            public string Type {
                get { return this.helper.type; }
            }

            public IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
