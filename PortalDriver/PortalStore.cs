﻿using System;
using System.Diagnostics;
using ClickPortal.Pages;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    class PortalStore : IPortalStore
    {
        private string storeUrlOrLabel;
        private PortalDriver portalDriver;

        private const string LogoffUrlPath = "/Rooms/RoomComponents/LoginView/Logoff";
        private const string LoginUrlpath = "/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5B0A7646F3B149874E902185897C144551%5D%5D";
        private const string LoginLoginPath = "/Login/Login";

        private string currentUser;

        public string CurrentUser
        {
            get { return currentUser; }
        }


        internal PortalStore(PortalDriver driver, string storeUrlOrLabel)
        {
            this.portalDriver = driver;
            this.storeUrlOrLabel = storeUrlOrLabel;
        }

        //requires a config object
        public void Login()
        {
            var sc = this.portalDriver.getStoreConfig(this.storeUrlOrLabel);
            this.Login(this.portalDriver, sc.URL,sc.USER1, sc.Pass(sc.USER1));
        }

        //requires a config object
        public void Login(string user)
        {
            var sc = this.portalDriver.getStoreConfig(this.storeUrlOrLabel);
            this.Login(this.portalDriver, sc.URL, user, sc.Pass(user));
        }

        //config object or URL
        public void Login(string user, string password)
        {
            var url = this.portalDriver.getBaseURL(this.storeUrlOrLabel);
            this.Login(this.portalDriver, url, user, password);
        }

        public void LoginAdmin()
        {
            var sc = this.portalDriver.getStoreConfig(this.storeUrlOrLabel);
            this.Login(this.portalDriver, sc.URL, sc.ADMIN_USER, sc.ADMIN_PASS);
        }

        public string Execute(string script)
        {
            var conf = this.portalDriver.getStoreConfig(this.storeUrlOrLabel);
            return this.Execute(script, conf.ADMIN_USER, conf.ADMIN_PASS);
        }

        public string Execute(string script, string user, string pass)
        {
            var url = this.portalDriver.getBaseURL(this.storeUrlOrLabel);
            var driver = this.portalDriver.AdminDriver;
            if (driver.Console(url).Go()) return driver.Console(url).Execute(script) ;
            
            this.Login(driver, url, user, pass);
            driver.Console(url).Go();
            return driver.Console(url).Execute(script);
        }

        public void Logoff()
        {
            this.Logoff(this.portalDriver, this.portalDriver.getBaseURL(this.storeUrlOrLabel));
        }

        public void Logoff(PortalDriver driver, string url)
        {            
            driver.Navigate().GoToUrl(url+LogoffUrlPath);
        }

        private void Login(PortalDriver driver, string url, string user, string pass)
        {
            PortalJsonDriverConfiguration.StoreConfig config = this.portalDriver.getStoreConfig(this.storeUrlOrLabel);

            this.Logoff(driver, url);
            string path = config.useLoginLogin ? LoginLoginPath : LoginUrlpath;

            driver.Navigate().GoToUrl(url+path);
            driver.FindElement(By.Name("username")).SendKeys(user);
            driver.FindElement(By.Name("password")).SendKeys(pass);
            IWebElement loginLink = driver.FindElement(By.XPath("//input[@value='Login']"));
            loginLink.Click();

            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.LinkText("Logoff")));
            this.currentUser = user;
        }

        public void Import()
        {
            throw new NotImplementedException();
        }
    }
}