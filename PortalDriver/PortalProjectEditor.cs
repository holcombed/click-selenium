﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    class PortalProjectEditor : IPortalProjectEditor
    {
        private By byTitleOrElem;
        private ISearchContext context;


        public PortalProjectEditor(By byTitleOrElem, ISearchContext context)
        {
            this.byTitleOrElem = byTitleOrElem;
            this.context = context;
        }

        public void Click()
        {
            this.context.FindElement(byTitleOrElem).Click();
        }

        public bool Is()
        {
            return this.context.FindElements(byTitleOrElem).Count != 0;
        }
    }
}
