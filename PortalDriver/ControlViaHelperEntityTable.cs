﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    internal class ControlViaHelperEntityTable : IControlFactoryFromHelper
    {
        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityTable") return new ControlEntityTable(helper, driver);
            return null;
        }

        internal class ControlEntityTable : IPortalControl
        {

            private readonly ControlHelper helper;
            private IPortalDriver driver;

            internal ControlEntityTable(ControlHelper helper, IPortalDriver driver)
            {
                this.helper = helper;
                this.driver = driver;
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;//TODO: implement? This is a placeholder.
                }
            }

            public bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public bool Is(string textOrOID)
            {
                throw new NotImplementedException();
            }

            public string OID
            {
                get { throw new NotImplementedException(); }
            }

            public string Text
            {
                get { throw new NotImplementedException(); }
            }

            public bool IsClear
            {
                get { throw new NotImplementedException(); }
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IPortalControlValue this[int index]
            {
                get { throw new NotImplementedException(); }
            }

            public int Count
            {
                get { throw new NotImplementedException(); }
            }

            public bool Is()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Clear()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val(string TextOrOID)
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }

            public string Type
            {
                get { return this.helper.type; }
            }

            public IWebElement AddButton
            {
                get
                {
                    var widgets = this.helper.widgets;
                    if (widgets != null)
                    {
                        for (var i = 0; i < widgets.Count; i++)
                        {
                            var elem = widgets[i];
                            var elemId = elem.GetAttribute("id");
                            if (elemId != null && elemId.EndsWith("_addBtn"))
                            {
                                return elem;
                            }
                        }
                    }
                    return null;
                }
            }
        }
    }
}
