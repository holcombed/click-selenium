﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace ClickPortal.PortalDriver {
    internal class PortalControlValue : IPortalControlValue
    {
        public PortalControlValue(string OID, string Text, bool IsClear)
        {
            this.OID = OID;
            this.Text = Text;
            this.IsClear = IsClear;
        }

        public bool Is(IPortalControlValue value)
        {
            return this.OID == value.OID;
        }

        public bool Is(string textOrOID)
        {
            return (this.OID == textOrOID || this.Text == textOrOID);
        }

        public override bool Equals(object obj)
        {
            var val = obj as IPortalControlValue;
            if (val == null) return false;

            return (
                this.IsClear == val.IsClear &&
                this.OID == val.OID &&
                this.Text == val.Text
                );
        }

        public override int GetHashCode()
        {
            return OID.GetHashCode();
        }

        public override string ToString() {return this.Text + " <" + this.OID + ">";}

        public string OID     { get; private set; }
        public string Text    { get; private set; }
        public bool   IsClear   { get; private set; }
    }


    internal class PortalControlValues : PortalControlValue, IPortalControlValues
    {

        private IPortalControlValue[] values = new PortalControlValue[]{};

        public PortalControlValues(string OID, string Text, bool isClear, IPortalControlValue[] values) : base(OID, Text, isClear)
        {
            this.values = values;
        }


        public IEnumerator<IPortalControlValue> GetEnumerator() {
            return this.values.AsEnumerable().GetEnumerator();
        }


        public IPortalControlValue this[int index]
        {
            get { return this.values[index]; }
        }

        public int Count
        {
            get { return this.values.Length; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.values.AsEnumerable().GetEnumerator();
        }
    }

}