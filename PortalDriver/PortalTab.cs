﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.PortalDriver
{
    public class PortalTab : IPortalTab
    {
        private readonly By _searchBy;
        private readonly ISearchContext _context;

        public PortalTab(By searchBy, ISearchContext context)
        {
            _searchBy = searchBy;
            _context = context;
        }

        public bool Is()
        {
            return this._context.FindElements(_searchBy).Count != 0;
        }
        
        public void Click()
        {
            this._context.FindElement(_searchBy).Click();
        }
    }
}
