﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    internal class ControlViaHelperEntityCheckbox : IControlFactoryFromHelper
    {
        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityCheckbox") return new ControlEntityCheckbox(helper, driver);
            return null;
        }

        internal class ControlEntityCheckbox : IPortalControl
        {

            private readonly ControlHelper helper;
            private IPortalDriver driver;

            internal ControlEntityCheckbox(ControlHelper helper, IPortalDriver driver)
            {
                this.helper = helper;
                this.driver = driver;
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            
            public bool HasHelpBubble
            {
                get
                {
                    return false;//TODO: implement? This is a placeholder.
                }
            }

            public bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public bool Is(string textOrOID)
            {
                throw new NotImplementedException();
            }

            public string OID
            {
                get { throw new NotImplementedException(); }
            }

            public string Text
            {
                get { throw new NotImplementedException(); }
            }

            public bool IsClear
            {
                get { return this.helper.OID == null || this.helper.OID.Trim() == ""; }
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IPortalControlValue this[int index]
            {
                get { throw new NotImplementedException(); }
            }

            public int Count
            {
                get { throw new NotImplementedException(); }
            }

            public bool Is()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Clear()
            {
                var checkedValues = this.helper.Values;
                // Iterate checked items and uncheck each item
                for (var i = 0; i < checkedValues.Count; i++)
                {
                    var oid = checkedValues[i].OID;
                    this.Val(oid);  // uncheck item
                }
                return null;
            }

            public IPortalControlValue Val()
            {
                if (this.IsClear) return new PortalControlValue(null, "", true);
                return new PortalControlValue(this.helper.OID, this.helper.text.Trim(), false);
            }

            public IPortalControlValue Val(string TextOrOID)
            {
                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                string shortestMatchingText = null;
                var index = 0;

                for (var i = 0; i < this.helper.rangeOID.Count; i++)
                {
                    var oid = this.helper.rangeOID[i];
                    var text = this.helper.rangeText[i];

                    if (oid == TextOrOID)
                    {
                        index = i;
                        break;
                    }

                    if (!text.Contains(TextOrOID)) continue;
                    if (shortestMatchingText != null && shortestMatchingText.Length <= text.Length) continue;
                    shortestMatchingText = text;
                    index = i;
                }

                this.helper.widgets[index].Click();
                return null;
            }

            public IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }

            public string Type
            {
                get { throw new NotImplementedException(); }
            }

            public IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
