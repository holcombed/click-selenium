﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    public class PortalTopNavTab : IPortalTopNavTab
    {
        private readonly By _searchBy;
        private readonly ISearchContext _context;

        public PortalTopNavTab(By searchBy, ISearchContext context)
        {
            _searchBy = searchBy;
            _context = context;
        }

        public bool Is()
        {
            return this._context.FindElements(_searchBy).Count != 0;
        }

        public void Click()
        {
            this._context.FindElement(_searchBy).Click();
        }
    }
}
