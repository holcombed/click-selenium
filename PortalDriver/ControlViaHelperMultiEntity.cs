﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperMultiEntity : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityTable") return new ControlMultiEntity(helper, driver);
            return null;
        }

        internal class ControlMultiEntity : IPortalControl
        {
            private readonly ControlHelper helper;
            private IPortalDriver driver;

            public string Type
            {
                get
                {
                    return this.helper.type;
                }
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public int Count
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public string OID
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public string Text
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public bool IsClear
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IWebElement AddButton
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IPortalControlValue this[int index]
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            internal ControlMultiEntity(ControlHelper helper, IPortalDriver driver)
            {
                this.helper = helper;
                this.driver = driver;
            }

            public bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public bool Is(string textOrOID)
            {
                throw new NotImplementedException();
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public bool Is()
            {
                return true;
            }

            public IPortalControlValue Clear()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val()
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val(string TextOrOID)
            {
                throw new NotImplementedException();
            }

            public IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }
        }
    }
}
