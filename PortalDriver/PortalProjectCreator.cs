﻿using System.Runtime.CompilerServices;
using System.Threading;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    class PortalProjectCreator : IPortalProjectCreator
    {
        private By byTitleOrElem;
        private ISearchContext context;

        
        public PortalProjectCreator(By byTitleOrElem, ISearchContext context)
        {
            this.byTitleOrElem = byTitleOrElem;
            this.context = context;
        }

        public void Click()
        {
            this.context.FindElement(byTitleOrElem).Click();
        }

        public bool Is()
        {
            return this.context.FindElements(byTitleOrElem).Count != 0;
        }
    }
}
