﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.PortalDriver
{
    public class ProjectListing : IPortalProjectListing
    {
        private readonly By _searchBy;
        private readonly PortalDriver _context;

        public ProjectListing(By searchBy, PortalDriver context)
        {
            _searchBy = searchBy;
            _context = context;
        }

        public bool Is()
        {
            return this._context.FindElements(_searchBy).Count != 0;
        }

        public void ClickProjectIdOrNameToWorkspace(string projectIdOrName)
        {
            var projectIdPath =
                "//div[contains(@id,'component')]/table/tbody/tr/td/span[@class='textControl' and text()='" + projectIdOrName +
                "']";
            _context.FindElement(By.XPath("." + projectIdPath));

            var projectLinkToWorkspacePath = projectIdPath +
                                             "/ancestor::tr[1]//a[contains(@href, 'Rooms/DisplayPages/LayoutInitial?Container=')]";
            var projectLink = _context.FindElement(By.XPath("." + projectLinkToWorkspacePath));
            projectLink.Click();

        }

        public void FilterBy(string queryText, string filterBy)
        {
            const string filterControlPath = "//td[contains(@id, '_filterControl')]";

            var filterOptionPath = "//select/option[text()='" + filterBy + "']";

            var filterDropDownOption = _context.FindElement(By.XPath("." + filterOptionPath));
            filterDropDownOption.Click();

            var filterCriteriaBy = By.XPath(".//input[contains(@id,'_queryCriteria')]");
            var filterCriteria = _context.FindElement(filterCriteriaBy);

            new WebDriverWait(_context, TimeSpan.FromSeconds(30)).Until(
                ExpectedConditions.ElementToBeClickable(filterCriteriaBy));

            filterCriteria.Clear();
            filterCriteria.SendKeys(queryText);

            _context.FindElement(By.XPath("." + filterControlPath + "/input[@value='Go']")).Click();

            new WebDriverWait(_context, TimeSpan.FromSeconds(30)).Until(
                ExpectedConditions.ElementToBeClickable(filterCriteriaBy));
        }
    }
}
