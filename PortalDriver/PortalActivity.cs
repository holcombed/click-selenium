﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    public class PortalActivity : IPortalActivity
    {
        private IPortalDriver Driver;
        private const string ACTIVITY_FORM_SD = "ResourceAdministration/Activity/form";
        private const string PROJECT_VALIDATION_SD = "ResourceAdministration/Project/ValidateProject";
        public bool ValidateBeforeExecution { get; private set; }
        internal string Locator;

        public PortalActivity(IPortalDriver driver, string locator)
        {
            Driver = driver;
            Locator = locator;
        }

        private IWebElement GetActivityLinkElement()
        {
            try
            {
                return Driver.FindElement(By.XPath("//a[contains(@href, 'Activity') and text()='" + Locator + "']"));
            }
            catch (NoSuchElementException)
            {
                return GetActivityLinkElementByInternalName();
            }
        }

        private IWebElement GetActivityLinkElementByInternalName()
        {
            string script = "? ApplicationEntity.getResultSet(\"ActivityType\").query(\"id = '" + Locator + "'\").elements().item(1);";
            string output = Driver.Store().Execute(script);
            output = output.Replace("[", "%5B").Replace("]", "%5D");
            return Driver.FindElement(By.XPath("//a[contains(@href,'ActivityType=" + output + "')]"));
        }

        public void Open()
        {
            Driver.Window().New(GetActivityLinkElement().Click);
        }

        public void Submit()
        {
            Driver.Window().Close(Driver.FindElement(By.Id("okBtn")).Click);
        }

        public void Cancel()
        {
            Driver.Window().Close(Driver.FindElement(By.Id("cancelBtn")).Click);
        }

        public bool Is()
        {
            try
            {
                GetActivityLinkElement();
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void OpenAndSubmit()
        {
            Open();
            Submit();
        }
    }
}
