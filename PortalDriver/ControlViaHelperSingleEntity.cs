﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperSingleEntity : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver) {
            if ("EntityRadio EntitySelect".Contains(helper.type)) return new ControlSingleEntity(helper);
            return null;
        }

        internal class ControlSingleEntity : IPortalControl {

            private readonly ControlHelper helper;

            internal ControlSingleEntity(ControlHelper helper) {
                this.helper = helper;
            }

            public bool Is(IPortalControlValue value) {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public bool Is(string textOrOID) {
                var text = textOrOID ?? "";
                if (text == "" && this.helper.text == "") return true;
                return this.helper.text.Trim().Contains(text.Trim());
            }

            public string OID {
                get { return this.helper.OID; }
            }

            public string Text {
                get { return this.helper.text; }
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public bool IsClear {
                get { return this.helper.OID == null || this.helper.OID.Trim() == ""; }
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                return this.helper.Values.GetEnumerator();
            }


            IEnumerator IEnumerable.GetEnumerator() {
                return this.GetEnumerator();
            }

            public IPortalControlValue this[int index] {
                get { return this.helper.Values[index]; }
            }

            public int Count {
                get { return this.helper.Values.Count; }
            }

            public bool Is() {
                return true;
            }

            public IPortalControlValue Clear()
            {
                if (this.Type == "EntitySelect") new SelectElement(this.helper.widgets.Last()).SelectByIndex(0);
                if (this.Type == "EntityRadio") this.helper.widgets.Last().Click();
                return null;
            }

            public IPortalControlValue Val() {
                if (this.IsClear) return new PortalControlValue(null, "", true);
                return new PortalControlValue(this.helper.OID, this.helper.text.Trim(), false);
            }

            public IPortalControlValue Val(string TextOrOID) {

                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                string shortestMatchingText = null;
                var index = 0;

                for (var i=0; i < this.helper.rangeOID.Count; i++)
                {
                    var oid = this.helper.rangeOID[i];
                    var text = this.helper.rangeText[i];

                    if (oid == TextOrOID)
                    {
                        index = i;
                        break;
                    }

                    if (!text.Contains(TextOrOID)) continue;
                    if (shortestMatchingText != null && shortestMatchingText.Length <= text.Length) continue;
                    shortestMatchingText = text;
                    index = i;
                }

                if(this.Type == "EntitySelect") new SelectElement(this.helper.widgets.Last()).SelectByIndex(index);
                if (this.Type == "EntityRadio") this.helper.widgets[index].Click();

                return null;
            }

            public IPortalControlValue Val(DateTime date)
            {
                this.Val(date.ToString());
                return null;
            }

            public string Type {
                get { return this.helper.type; }
            }

            public IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
