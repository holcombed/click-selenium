﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    class ControlViaHelperBoolean : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            return !helper.type.Contains("Boolean") ? null : new ControlBoolean(helper);
        }


        class ControlBoolean : IPortalControl
        {
            private ControlHelper helper;

            private Boolean truthyFalsy(string testMe)
            {
                if (testMe == null) return false;
                if (testMe == "") return false;
                testMe = testMe.Trim().ToUpper();
                if (testMe == "TRUE" || testMe == "YES") return true;
                if (testMe == "FALSE" || testMe == "NO") return false;
                throw new ArgumentException("Could not evaluate the truthy/falsy of" + testMe);
            }
 
            public ControlBoolean(ControlHelper helper)
            {
                this.helper = helper;
            }

            public bool Is(IPortalControlValue value)
            {
                if (value.OID != null) return false;
                return truthyFalsy(helper.text) == truthyFalsy(value.Text);
            }

            public bool Is(string textOrOID)
            {
                if (string.IsNullOrEmpty(textOrOID)) return this.IsClear;
                return truthyFalsy(textOrOID) == truthyFalsy(this.helper.OID);
            }

            public string OID
            {
                get { return this.helper.OID; }
            }

            public string Text
            {
                get { return this.helper.text; }
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public bool IsClear
            {
                get {return string.IsNullOrEmpty(this.helper.OID);}
            }


            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                return this.helper.Values.GetEnumerator();
            }

            public IPortalControlValue this[int index]
            {
                get { return this.helper.Values[index]; }
            }

            public int Count
            {
                get { return this.helper.Values.Count; }
            }

            public bool Is()
            {
                return true;
            }

            public IPortalControlValue Val()
            {
                return new PortalControlValue(this.OID, this.Text, this.IsClear);
            }

            public IPortalControlValue Val(string TextOrOID)
            {
                if (string.IsNullOrEmpty(TextOrOID)) { this.Clear(); } 
                else { setControlBoolean(this.truthyFalsy(TextOrOID)); }

                return null;
            }

            public IPortalControlValue Clear()
            {
                if (this.IsClear) return null;
                if (this.helper.type == "BooleanCheckbox") this.setControlBoolean(false);
                if (this.helper.type == "BooleanRadio") this.helper.widgets.Last().Click();
                if (this.helper.type == "BooleanSelect") new SelectElement(this.helper.widgets.Last()).SelectByIndex(0);
                return null;
            }

            private void setControlBoolean(Boolean trueFalse)
            {
                var currentValue = this.Is("TRUE");

                if (this.helper.type == "BooleanCheckbox" && trueFalse != currentValue)
                {
                    this.helper.widgets[0].Click();
                    return;
                }

                if (trueFalse == currentValue && !this.IsClear) return;

                if (this.helper.type == "BooleanSelect")
                {
                    var selector = new SelectElement(this.helper.widgets.Last());
                    var selectIndex = 0;
                    for (; selectIndex<this.helper.rangeOID.Count; selectIndex++)
                    {
                        var optionValue = this.helper.rangeOID[selectIndex];
                        if (string.IsNullOrEmpty(optionValue)) continue;
                        if (truthyFalsy(optionValue) == trueFalse) break;
                    }
                    selector.SelectByIndex(selectIndex);
                    return;
                }

                if (this.helper.type == "BooleanRadio")
                {
                    var selectIndex = 0;

                    for (; selectIndex < this.helper.rangeOID.Count; selectIndex++) {
                        var optionValue = this.helper.rangeOID[selectIndex];
                        if (optionValue == null) continue;
                        if (truthyFalsy(optionValue) == trueFalse) break;
                    }
                    this.helper.widgets[selectIndex].Click();
                    return;
                }
            }
            

            public IPortalControlValue Val(DateTime date)
            {
                throw new NotSupportedException("Date cannot be set to boolean Control");
            }


            public string Type
            {
                get { return this.helper.type; }
            }

            public IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }
    }
}