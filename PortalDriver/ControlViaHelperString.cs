﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperString : IControlFactoryFromHelper
    {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            return "DatePicker Input Textarea".Contains(helper.type) ? new ControlString(helper) : null;
        }

        internal class ControlString : IPortalControl
        {

            private ControlHelper helper;

            internal ControlString(ControlHelper helper)
            {
                this.helper = helper;
            }

            public bool Is(IPortalControlValue value)
            {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public bool Is(string textOrOID)
            {
                var text = textOrOID ?? "";
                if (text == "" && this.helper.text == "") return true;
                return this.helper.text.Trim().Contains(text.Trim());
            }

            public string OID
            {
                get { return null; }
            }

            public string Text
            {
                get { return this.helper.text; }
            }

            public string Caption
            {
                get
                {
                    return this.helper.captionText;
                }
            }

            public bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public bool IsClear
            {
                get { return this.helper.text.Trim() == ""; }
            }

            public IEnumerator<IPortalControlValue> GetEnumerator()
            {
                return this.createValuesArray().GetEnumerator();
            }

            private ReadOnlyCollection<IPortalControlValue> createValuesArray()
            {
                return this.IsClear ? 
                    new ReadOnlyCollection<IPortalControlValue>(new IPortalControlValue[] {}) : 
                    new ReadOnlyCollection<IPortalControlValue>(new IPortalControlValue[]{new PortalControlValue(null,this.helper.text.Trim(),false)});
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            public IPortalControlValue this[int index]
            {
                get { return this.createValuesArray()[index]; }
            }

            public int Count
            {
                get { return this.createValuesArray().Count; }
            }

            public bool Is()
            {
                return true;
            }

            public IPortalControlValue Clear()
            {
                this.helper.widgets[0].SendKeys(Keys.Control + "a");
                this.helper.widgets[0].SendKeys(Keys.Delete);
                return null;
            }

            public IPortalControlValue Val()
            {
                if (this.IsClear) new PortalControlValue(null, "", true);
                return new PortalControlValue(null, this.helper.text.Trim(), false);
            }

            public IPortalControlValue Val(string TextOrOID)
            {
                this.Clear();
                this.helper.widgets[0].SendKeys(TextOrOID);
                return null;
            }

            public IPortalControlValue Val(DateTime date)
            {
                this.Clear();
                this.helper.widgets[0].SendKeys(date.ToString());
                return null;
            }

            public string Type
            {
                get { return this.helper.type; }
            }

            public IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
