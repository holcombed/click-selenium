﻿using System;
using OpenQA.Selenium;

namespace ClickPortal.BaseTypes
{
    public class ProjectCreator
    {
        private const string PROJECT_CREATOR_CSS_SELECTOR = "[onclick^='createProject']";
        private readonly int _index;
        private readonly string _title;
        private readonly string _altText;
        private readonly string _imageFileName;
        private readonly IWebDriver _driver;
        private IWebElement _linkElement;
        public IWebElement LinkElement
        {
            get
            {
                if (_linkElement != null)
                {
                    return _linkElement;
                }
                // Initialize _element
                if (_index > -1)
                {
                    _linkElement = GetProjectCreatorByIndex();
                }
                else if (_title != null)
                {
                    _linkElement = GetProjectCreatorByTitle();
                }
                else if (_altText != null)
                {
                    _linkElement = GetProjectCreatorByAltText();
                }
                else if (_imageFileName != null)
                {
                    _linkElement = GetProjectCreatorByImageFileName();
                }
                return _linkElement;
            }
        }

        private IWebElement GetProjectCreatorByIndex()
        {
            var creators = _driver.FindElements(By.CssSelector(PROJECT_CREATOR_CSS_SELECTOR));
            return creators[_index];
        }

        private IWebElement GetProjectCreatorByTitle()
        {
            return _driver.FindElement(By.CssSelector("[title='" + _title + "']" + PROJECT_CREATOR_CSS_SELECTOR));
        }

        private IWebElement GetProjectCreatorByAltText()
        {
            return _driver.FindElement(By.CssSelector("[alt='" + _altText + "']" + PROJECT_CREATOR_CSS_SELECTOR));
        }

        private IWebElement GetProjectCreatorByImageFileName()
        {
            return _driver.FindElement(By.CssSelector("[src*='" + _imageFileName + "']" + PROJECT_CREATOR_CSS_SELECTOR));
        }

        public ProjectCreator(IWebDriver driver, int index = -1, string title = null, string altText = null, string imageFileName = null)
        {
            if (index == -1 && title == null && altText == null && imageFileName == null)
            {
                throw new Exception("You need to pass at least one of the optional parameters.");
            }
            _driver = driver;
            _index = index;
            _title = title;
            _altText = altText;
            _imageFileName = imageFileName;
        }

        public void ClickLink()
        {
            LinkElement.Click();
        }
    }
}
