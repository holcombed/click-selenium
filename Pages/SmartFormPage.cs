﻿using System.Linq;
using OpenQA.Selenium;

namespace ClickPortal.Pages
{
    public abstract class SmartFormPage : PageController
    {
        public const string SAVE_LINK_ID = "lnkSaveProjectEditor_Top";
        public const string EXIT_LINK_ID = "lnkExitProjectEditor_Top";
        public const string CONTINUE_BUTTON_ID = "continue_btn_Top";
        public const string BACK_BUTTON_ID = "back_btn_Top";
        public const string FINISH_BUTTON_ID = "finish_btn_Top";
        public const string JUMP_TO_LINK_ID = "jumpToMenuLink_Top";
        public const string JUMP_TO_DIV_ID = "jumpToMenuDiv_Top";
        public const string JUMP_TO_ONCLICK_METHOD = "onChangeJumpToMenu";
        public void Save()
        {
            driver.FindElement(By.Id(SAVE_LINK_ID)).Click();
        }

        public void Exit()
        {
            driver.FindElement(By.Id(EXIT_LINK_ID)).Click();
        }

        public void Continue()
        {
            driver.FindElement(By.Id(CONTINUE_BUTTON_ID)).Click();
        }

        public void Back()
        {
            driver.FindElement(By.Id(BACK_BUTTON_ID)).Click();
        }

        public void Finish()
        {
            driver.FindElement(By.Id(FINISH_BUTTON_ID)).Click();
            driver.Window().Close();
        }

        public void JumpTo(string sectionOrStepName)
        {
            var jumpToLink = driver.FindElement(By.Id(JUMP_TO_LINK_ID));
            jumpToLink.Click();

            WaitForElementToBeVisible(By.Id(JUMP_TO_DIV_ID));
            var linksWithText = driver.FindElements(By.PartialLinkText(sectionOrStepName));
            var stepLink = linksWithText.First(d => ((string)d.GetAttribute("onclick")).StartsWith(JUMP_TO_ONCLICK_METHOD));
            stepLink.Click();
        }

        public void SaveAndExit()
        {
            Save();
            Exit();
        }

        public abstract void FillOutUsingTestData();
    }
}
