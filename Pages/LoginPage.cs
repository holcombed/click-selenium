﻿using System;
using OpenQA.Selenium;

namespace ClickPortal.Pages
{
    public class LoginPage : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/");
            if (driver.Title != "Login")
            {
                Logoff();
                IWebElement loginLink = driver.FindElement(By.CssSelector("input[class='Button'][value='Login']"));
                loginLink.Click();
            }
        }

        public void Login(String username, String password)
        {
            if (driver.Title != "Login")
            {
                throw new Exception("Attempt to log in to non-login page.");
            }
            IWebElement un = GetUsernameElement();
            un.SendKeys(username);

            IWebElement pw = GetPasswordElement();
            pw.SendKeys(password);

            un.Submit();
        }

        protected IWebElement GetUsernameElement()
        {
            return driver.FindElement(By.Name("username"));
        }

        protected IWebElement GetPasswordElement()
        {
            return driver.FindElement(By.Name("password"));
        }

        public void Logoff()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5B0A7646F3B149874E902185897C144551%5D%5D&_webrlogoff=true");
        }
    }
}