﻿namespace ClickPortal.Pages
{
    public abstract class FolderPage : PageController
    {
        public abstract string PageName { get; }

        public override void Navigate()
        {
            ClickNavigatorTab(PageName);
        }
    }
}
