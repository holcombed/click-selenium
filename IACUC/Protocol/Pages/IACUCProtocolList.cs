﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolList : PageController
    {
        public struct Result
        {
            public String ID;
            public String Name;
            public String Modified;
            public String State;
            public String SubmissionType;
            public String PIFirstName;
            public String PILastName;
        }

        private Dictionary<String, String> _ComponentIDTabs = new Dictionary<string, string>{
            { "In-Review", "E0318D58A4FCE64B98673D58D1CA51EF" },
            { "Active", "A7BAEE9FA050B345A52D8A330CFB8AC6" },
            { "Archived", "FC49C958D22AA64BAE92D5FEADFE2553" },
            { "Research Teams", "727849528C860541BD1CF4649A7DA6B6" },
            { "All Submissions", "13100587638EDE48A514B91CCAA8550E" }
        };

        private String _ComponentID;

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5B9EB5178212175C439C7FFDF200BA9EFE%5D%5D");
            _ComponentID = _ComponentIDTabs["Research Teams"];
        }
        public void NavigateAllSubmissions()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[9EB5178212175C439C7FFDF200BA9EFE]]&Tab2=com.webridge.entity.Entity[OID[13100587638EDE48A514B91CCAA8550E]]");
            _ComponentID = _ComponentIDTabs["All Submissions"];
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.XPath("//input[@name='_webrUpdateOID' and @value='com.webridge.entity.Entity[OID[" + _ComponentID + "]]']")));
        }

        public string FilterBy
        {
            get
            {
                SelectElement filterSelect = new SelectElement(driver.FindElement(By.Id("component" + this._ComponentID + "_queryField1")));
                return filterSelect.SelectedOption.Text;
            }

            set
            {
                SelectElement filterSelect = new SelectElement(driver.FindElement(By.Id("component" + this._ComponentID + "_queryField1")));
                filterSelect.SelectByText(value);
            }
        }

        public string FilterText
        {
            get
            {
                IWebElement textField = driver.FindElement(By.Id("component" + this._ComponentID + "_queryCriteria1"));
                return textField.GetAttribute("value");
            }
            set
            {
                IWebElement textField = driver.FindElement(By.Id("component" + this._ComponentID + "_queryCriteria1"));
                new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until((_driver) => { return textField.Enabled; });
                textField.Clear();
                new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until((_driver) => { return textField.Enabled; });
                textField.SendKeys(value);
            }
        }

        public void Filter()
        {
            IWebElement tableBefore = driver.FindElement(By.XPath("//div[@id='component" + this._ComponentID + "']/table"));
            driver.FindElement(By.Id("component" + this._ComponentID + "_requery")).Click();
            // Wait until the table has refreshed.
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until((_driver) => { try { bool _enabled = tableBefore.Enabled; return false; } catch (StaleElementReferenceException) { return true; } });
        }

        public void Filter(String by, String token)
        {
            this.FilterBy = by;
            this.FilterText = token;
            this.Filter();
        }
    }
}
