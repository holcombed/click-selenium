﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureProcedureIdentificationForm : IACUCProcedureFormPage
    {
        public IACUCProcedureProcedureIdentificationForm(IACUCProcedure procedure)
            : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[E995F72E524D8849B070E86D5025B6FC]]");
        }
    }
}
