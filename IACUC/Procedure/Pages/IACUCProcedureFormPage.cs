﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureFormPage : PageController
    {
        protected IACUCProcedure _procedure;

        public IACUCProcedureFormPage(IACUCProcedure procedure)
        {
            _procedure = procedure;
        }

        public bool isEditable()
        {
            return driver.FindElements(By.Id("InkSaveProjectEditor")).Count > 0;
        }

        public IWebElement SaveLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Save"));
            }
        }

        public IWebElement ExitLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Exit"));
            }
        }
    }
}
