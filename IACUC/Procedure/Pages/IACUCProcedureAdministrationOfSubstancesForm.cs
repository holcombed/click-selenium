﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureAdministrationOfSubstancesForm : IACUCProcedureFormPage 
    {
        public IACUCProcedureAdministrationOfSubstancesForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[5174E2EF199DF7448B518A54BA3ED1C3]]");
        }
    }
}
