﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureBehavioralProceduresForm : IACUCProcedureFormPage
    {
        public IACUCProcedureBehavioralProceduresForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[295B3A58986CA04CBF8DD5D8983C55AC]]");
        }
    }
}
