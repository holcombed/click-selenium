﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureWorkspace : PageController
    {
        public String OID;
        private String _projectOid;

        public IACUCProcedureWorkspace(String Oid)
        {
            this.OID = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=" + OID);
        }

        public String Status
        {
            get
            {
                return driver.FindElement(By.XPath("/*[@id='__ClickProcedure.status_container']/span[3]/span/span[1]")).Text;
            }
        }

        public void OpenActivity(String LinkText)
        {
            IWebElement anchor = driver.FindElement(By.LinkText(LinkText));
            OpenPopup(anchor, null, By.XPath("//td[@class='FormHead']"));
        }

        public String projectOid
        {
            get
            {
                if (_projectOid == default(String))
                {
                    IWebElement scriptTag = driver.FindElement(By.XPath("//script[contains(.,'PortalTools.currentResource')]"));
                    String script = scriptTag.GetAttribute("innerHTML");
                    Regex regex = new Regex(@"PortalTools.currentResource = '(com.webridge.entity.Entity\[OID\[[0-9A-F]{32}\]\])';");
                    Match m = regex.Match(script);
                    if (!m.Success)
                    {
                        throw new Exception("Problem finding procedure OID.");
                    }
                    _projectOid = m.Groups[1].Value;
                }

                return _projectOid;
            }
        }
    }
}
