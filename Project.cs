﻿using ClickPortal.PortalDriver;
using OpenQA.Selenium;

namespace ClickPortal
{
    public abstract class Project
    {
        public string ProjectId { get; protected set; }
        public readonly IPortalDriver Driver;

        protected Project(IPortalDriver driver, string projectId = null)
        {
            Driver = driver;
            ProjectId = projectId;
        }

        public abstract string ProjectTypeDisplayName();

        public abstract void NavigateToWorkspace();

        public abstract string GetCurrentState();

        public abstract bool IsWorkspace();
    }
}
